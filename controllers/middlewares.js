module.exports = {
  logguerController: (req, res, next) => {
    //ignored patterns
    const ignoredPatterns = [
      "/healthcheck",
      "/metrics"
    ];

    //if (ignoredPatterns.includes(req.originalUrl)) return next();
    
    console.log(req.method.toUpperCase(), req.originalUrl);
    next();
  },
};