const {Server} = require("socket.io");
const { instrument } = require("@socket.io/admin-ui");
const { createAdapter } = require("@socket.io/postgres-adapter");
const { Pool } = require("pg");

const config = require("../config");

const io = new Server(global.server, {
  cors: {
    origin: config.socketio.cors,
    credentials: true,
    allowedHeaders: ["Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"],
    methods: ["GET", "POST"],
  }
});
global.io = io;

instrument(io, config.socketio.admin);

const [,,,user, password, host, port, database] = config.env.DATABASE_URL.split(/[:@/]/g);

const pool = new Pool({
  user: user,
  host: host,
  database: database,
  password: password,
  port: port,
});

pool.query(`
  CREATE TABLE IF NOT EXISTS socket_io_attachments (
      id          bigserial UNIQUE,
      created_at  timestamptz DEFAULT NOW(),
      payload     bytea
  );
`);

io.adapter(createAdapter(pool));
