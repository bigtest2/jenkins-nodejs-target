const fs = require("fs");
const env = require("../config/env");

//syncronize config/env.js to .env
let envFile = "";
Object.keys(env).forEach((key) => {
  envFile += `${key}=${env[key]}\n`;
});

if (!fs.existsSync(".env") || fs.readFileSync(".env", "utf-8") !== envFile) {
  fs.writeFileSync(".env", envFile);
}

require("dotenv").config();